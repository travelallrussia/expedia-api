# Swagger\Client\ShoppingApi

All URIs are relative to *https://api.ean.com/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**propertiesAvailabilityGet**](ShoppingApi.md#propertiesAvailabilityGet) | **GET** /properties/availability | Get property room rates and availability
[**propertiesPropertyIdPaymentOptionsGet**](ShoppingApi.md#propertiesPropertyIdPaymentOptionsGet) | **GET** /properties/{property_id}/payment-options | Get Accepted Payment Types - EPS MOR Only
[**propertiesPropertyIdRoomsRoomIdRatesRateIdGet**](ShoppingApi.md#propertiesPropertyIdRoomsRoomIdRatesRateIdGet) | **GET** /properties/{property_id}/rooms/{room_id}/rates/{rate_id} | Price-Check


# **propertiesAvailabilityGet**
> \Swagger\Client\Model\PropertyAvailability[] propertiesAvailabilityGet($accept, $accept_encoding, $authorization, $user_agent, $checkin, $checkout, $currency, $language, $country_code, $occupancy, $property_id, $sales_channel, $sales_environment, $rate_plan_count, $customer_ip, $customer_session_id, $test, $filter, $rate_option, $billing_terms, $payment_terms, $partner_point_of_sale, $platform_name)

Get property room rates and availability

Returns rates on available room types for specified properties (maximum of 250 properties per request).  The response includes rate details such as promos, whether the rate is refundable, cancellation penalties and a full price breakdown to meet the price display requirements for your market. _Note_: If there are no available rooms, the response will be an empty array. * Multiple rooms of the same type may be requested by including multiple instances of the `occupancy` parameter. * The `nightly` array includes each individual night's charges. When the total price includes fees, charges, or adjustments that are not divided by night, these amounts will be included in the `stay` rate array, which details charges applied to the entire stay (each check-in).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ShoppingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$checkin = "checkin_example"; // string | Check-in date, in ISO 8601 format (YYYY-MM-DD) Example: `2018-09-15`.
$checkout = "checkout_example"; // string | Check-out date, in ISO 8601 format (YYYY-MM-DD). Availability can be searched up to 500 days in advance of this date. Total length of stay cannot exceed 28 nights.  Example: `2018-09-17`.
$currency = "currency_example"; // string | Requested currency for the rates, in ISO 4217 format<br> Example: `USD`.<br> Currency Options: [https://developer.expediapartnersolutions.com/reference/currency-options/](https://developer.expediapartnersolutions.com/reference/currency-options/)
$language = "language_example"; // string | Desired language for the response as a subset of BCP47 format that only uses hyphenated pairs of two-digit language and country codes. Use only ISO639-1 alpha 2 language codes and ISO3166-1 alpha 2 country codes. See [https://www.w3.org/International/articles/language-tags/](https://www.w3.org/International/articles/language-tags/)  Example: `language=en-US`  Language Options: [https://developer.expediapartnersolutions.com/reference/language-options/](https://developer.expediapartnersolutions.com/reference/language-options/)
$country_code = "country_code_example"; // string | The country code of the traveler's point of sale, in ISO 3166-1 alpha-2 format. This should represent the country where the shopping transaction is taking place.  Example: country_code=US  For more information see: [https://www.iso.org/obp/ui/#search/code/](https://www.iso.org/obp/ui/#search/code/)
$occupancy = array("occupancy_example"); // string[] | Defines the requested occupancy for a single room. Each room must have at least 1 adult occupant.<br> Format: `numberOfAdults[-firstChildAge[,nextChildAge]]`<br> To request multiple rooms (of the same type), include one instance of occupancy for each room requested. Up to 8 rooms may be requested or booked at once.<br> Examples: * 2 adults, one 9-year-old and one 4-year-old would be represented by `occupancy=2-9,4`.<br> * A multi-room request to lodge an additional 2 adults would be represented by `occupancy=2-9,4&occupancy=2`
$property_id = array("property_id_example"); // string[] | The ID of the property you want to search for. You can provide 1 to 250 property_id parameters.  Example: `property_id=19248&property_id=20321`
$sales_channel = "sales_channel_example"; // string | You must provide the sales channel for the display of rates. EPS dynamically provides the best content for optimal conversion on each sales channel. If you have a sales channel that is not currently supported in this list, please contact our support team.<br> * `website` - Standard website accessed from the customer's computer * `agent_tool` - Your own agent tool used by your call center or retail store agent * `mobile_app` - An application installed on a phone or tablet device * `mobile_web` - A web browser application on a phone or tablet device * `meta` - Rates will be passed to and displayed on a 3rd party comparison website * `cache` - Rates will be used to populate a local cache
$sales_environment = "sales_environment_example"; // string | You must provide the sales environment in which rates will be sold. EPS dynamically provides the best content for optimal conversion. If you have a sales environment that is not currently supported in this list, please contact our support team.<br> * `hotel_package` - Use when selling the hotel with a transport product, e.g. flight & hotel. * `hotel_only` - Use when selling the hotel as an individual product. * `loyalty` - Use when you are selling the hotel as part of a loyalty program and the price is converted to points.
$rate_plan_count = 8.14; // float | The number of rates to return per property. The rates with the best value will be returned, e.g. a rate_plan_count=4 will return the best 4 rates, but the rates are not ordered from lowest to highest or vice versa in the response. Generally lowest rates will be prioritized.  The value must be between 1 and 250.
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | Shop calls have a test header that can be used to return set responses with the following keywords:<br> * `standard` * `service_unavailable` * `unknown_internal_error`
$filter = array("filter_example"); // string[] | Single filter type. Send multiple instances of this parameter to request multiple filters.<br> * `refundable` - Filters results to only show fully refundable rates. * `expedia_collect` - Filters results to only show rates where payment is collected by Expedia at the time of booking. These properties can be eligible for payments via Expedia Affiliate Collect(EAC). * `property_collect` - Filters results to only show rates where payment is collected by the property after booking. This can include rates that require a deposit by the property, dependent upon the deposit policies.  Example: `filter=refundable&filter=expedia_collect`
$rate_option = array("rate_option_example"); // string[] | Request specific rate options for each property. Send multiple instances of this parameter to request multiple rate options. Accepted values:<br> * `member` - Return member rates for each property. This feature must be enabled and requires a user to be logged in to request these rates. * `net_rates` - Return net rates for each property. This feature must be enabled to request these rates. * `cross_sell` - Identify if the traffic is coming from a cross sell booking. Where the traveler has booked another service (flight, car, activities...) before hotel.  Example: `rate_option=member&rate_option=net_rates`
$billing_terms = "billing_terms_example"; // string | This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately.
$payment_terms = "payment_terms_example"; // string | This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately.
$partner_point_of_sale = "partner_point_of_sale_example"; // string | This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately.
$platform_name = "platform_name_example"; // string | This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately.

try {
    $result = $apiInstance->propertiesAvailabilityGet($accept, $accept_encoding, $authorization, $user_agent, $checkin, $checkout, $currency, $language, $country_code, $occupancy, $property_id, $sales_channel, $sales_environment, $rate_plan_count, $customer_ip, $customer_session_id, $test, $filter, $rate_option, $billing_terms, $payment_terms, $partner_point_of_sale, $platform_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShoppingApi->propertiesAvailabilityGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **checkin** | **string**| Check-in date, in ISO 8601 format (YYYY-MM-DD) Example: &#x60;2018-09-15&#x60;. |
 **checkout** | **string**| Check-out date, in ISO 8601 format (YYYY-MM-DD). Availability can be searched up to 500 days in advance of this date. Total length of stay cannot exceed 28 nights.  Example: &#x60;2018-09-17&#x60;. |
 **currency** | **string**| Requested currency for the rates, in ISO 4217 format&lt;br&gt; Example: &#x60;USD&#x60;.&lt;br&gt; Currency Options: [https://developer.expediapartnersolutions.com/reference/currency-options/](https://developer.expediapartnersolutions.com/reference/currency-options/) |
 **language** | **string**| Desired language for the response as a subset of BCP47 format that only uses hyphenated pairs of two-digit language and country codes. Use only ISO639-1 alpha 2 language codes and ISO3166-1 alpha 2 country codes. See [https://www.w3.org/International/articles/language-tags/](https://www.w3.org/International/articles/language-tags/)  Example: &#x60;language&#x3D;en-US&#x60;  Language Options: [https://developer.expediapartnersolutions.com/reference/language-options/](https://developer.expediapartnersolutions.com/reference/language-options/) |
 **country_code** | **string**| The country code of the traveler&#39;s point of sale, in ISO 3166-1 alpha-2 format. This should represent the country where the shopping transaction is taking place.  Example: country_code&#x3D;US  For more information see: [https://www.iso.org/obp/ui/#search/code/](https://www.iso.org/obp/ui/#search/code/) |
 **occupancy** | [**string[]**](../Model/string.md)| Defines the requested occupancy for a single room. Each room must have at least 1 adult occupant.&lt;br&gt; Format: &#x60;numberOfAdults[-firstChildAge[,nextChildAge]]&#x60;&lt;br&gt; To request multiple rooms (of the same type), include one instance of occupancy for each room requested. Up to 8 rooms may be requested or booked at once.&lt;br&gt; Examples: * 2 adults, one 9-year-old and one 4-year-old would be represented by &#x60;occupancy&#x3D;2-9,4&#x60;.&lt;br&gt; * A multi-room request to lodge an additional 2 adults would be represented by &#x60;occupancy&#x3D;2-9,4&amp;occupancy&#x3D;2&#x60; |
 **property_id** | [**string[]**](../Model/string.md)| The ID of the property you want to search for. You can provide 1 to 250 property_id parameters.  Example: &#x60;property_id&#x3D;19248&amp;property_id&#x3D;20321&#x60; |
 **sales_channel** | **string**| You must provide the sales channel for the display of rates. EPS dynamically provides the best content for optimal conversion on each sales channel. If you have a sales channel that is not currently supported in this list, please contact our support team.&lt;br&gt; * &#x60;website&#x60; - Standard website accessed from the customer&#39;s computer * &#x60;agent_tool&#x60; - Your own agent tool used by your call center or retail store agent * &#x60;mobile_app&#x60; - An application installed on a phone or tablet device * &#x60;mobile_web&#x60; - A web browser application on a phone or tablet device * &#x60;meta&#x60; - Rates will be passed to and displayed on a 3rd party comparison website * &#x60;cache&#x60; - Rates will be used to populate a local cache |
 **sales_environment** | **string**| You must provide the sales environment in which rates will be sold. EPS dynamically provides the best content for optimal conversion. If you have a sales environment that is not currently supported in this list, please contact our support team.&lt;br&gt; * &#x60;hotel_package&#x60; - Use when selling the hotel with a transport product, e.g. flight &amp; hotel. * &#x60;hotel_only&#x60; - Use when selling the hotel as an individual product. * &#x60;loyalty&#x60; - Use when you are selling the hotel as part of a loyalty program and the price is converted to points. |
 **rate_plan_count** | **float**| The number of rates to return per property. The rates with the best value will be returned, e.g. a rate_plan_count&#x3D;4 will return the best 4 rates, but the rates are not ordered from lowest to highest or vice versa in the response. Generally lowest rates will be prioritized.  The value must be between 1 and 250. |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. | [optional]
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| Shop calls have a test header that can be used to return set responses with the following keywords:&lt;br&gt; * &#x60;standard&#x60; * &#x60;service_unavailable&#x60; * &#x60;unknown_internal_error&#x60; | [optional]
 **filter** | [**string[]**](../Model/string.md)| Single filter type. Send multiple instances of this parameter to request multiple filters.&lt;br&gt; * &#x60;refundable&#x60; - Filters results to only show fully refundable rates. * &#x60;expedia_collect&#x60; - Filters results to only show rates where payment is collected by Expedia at the time of booking. These properties can be eligible for payments via Expedia Affiliate Collect(EAC). * &#x60;property_collect&#x60; - Filters results to only show rates where payment is collected by the property after booking. This can include rates that require a deposit by the property, dependent upon the deposit policies.  Example: &#x60;filter&#x3D;refundable&amp;filter&#x3D;expedia_collect&#x60; | [optional]
 **rate_option** | [**string[]**](../Model/string.md)| Request specific rate options for each property. Send multiple instances of this parameter to request multiple rate options. Accepted values:&lt;br&gt; * &#x60;member&#x60; - Return member rates for each property. This feature must be enabled and requires a user to be logged in to request these rates. * &#x60;net_rates&#x60; - Return net rates for each property. This feature must be enabled to request these rates. * &#x60;cross_sell&#x60; - Identify if the traffic is coming from a cross sell booking. Where the traveler has booked another service (flight, car, activities...) before hotel.  Example: &#x60;rate_option&#x3D;member&amp;rate_option&#x3D;net_rates&#x60; | [optional]
 **billing_terms** | **string**| This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately. | [optional]
 **payment_terms** | **string**| This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately. | [optional]
 **partner_point_of_sale** | **string**| This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]
 **platform_name** | **string**| This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]

### Return type

[**\Swagger\Client\Model\PropertyAvailability[]**](../Model/PropertyAvailability.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **propertiesPropertyIdPaymentOptionsGet**
> \Swagger\Client\Model\PaymentOption propertiesPropertyIdPaymentOptionsGet($accept, $accept_encoding, $authorization, $property_id, $token, $user_agent, $customer_ip, $customer_session_id)

Get Accepted Payment Types - EPS MOR Only

Returns the accepted payment options.  Use this API to power your checkout page and display valid forms of payment, ensuring a smooth booking.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ShoppingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$property_id = "property_id_example"; // string | Expedia Property ID.<br> Example: `19248`
$token = "token_example"; // string | Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: `MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m`
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.

try {
    $result = $apiInstance->propertiesPropertyIdPaymentOptionsGet($accept, $accept_encoding, $authorization, $property_id, $token, $user_agent, $customer_ip, $customer_session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShoppingApi->propertiesPropertyIdPaymentOptionsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **property_id** | **string**| Expedia Property ID.&lt;br&gt; Example: &#x60;19248&#x60; |
 **token** | **string**| Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: &#x60;MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m&#x60; |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. | [optional]
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]

### Return type

[**\Swagger\Client\Model\PaymentOption**](../Model/PaymentOption.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **propertiesPropertyIdRoomsRoomIdRatesRateIdGet**
> \Swagger\Client\Model\RoomPriceCheck propertiesPropertyIdRoomsRoomIdRatesRateIdGet($accept, $accept_encoding, $authorization, $user_agent, $property_id, $room_id, $rate_id, $token, $customer_ip, $customer_session_id, $test)

Price-Check

Confirms the price returned by the Property Availability response. Use this API to verify a previously-selected rate is still valid before booking. If the price is matched, the response returns a link to request a booking. If the price has changed, the response returns new price details and a booking link for the new price. If the rate is no longer available, the response will return a new Property Availability request link to search again for different rates. In the event of a price change, go back to Property Availability and book the property at the new price or return to additional rates for the property.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ShoppingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$property_id = "property_id_example"; // string | Expedia Property ID.<br> Example: `19248`
$room_id = "room_id_example"; // string | Room ID of a property.<br> Example: `123abc`.
$rate_id = "rate_id_example"; // string | Rate ID of a room.<br> Example: `123abc`
$token = "token_example"; // string | A hashed collection of query parameters. Used to maintain state across calls. This token is provided as part of the price check link from the shop response.
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | Price check calls have a test header that can be used to return set responses with the following keywords:   * `available`   * `price_changed`   * `sold_out`   * `service_unavailable`   * `unknown_internal_error`

try {
    $result = $apiInstance->propertiesPropertyIdRoomsRoomIdRatesRateIdGet($accept, $accept_encoding, $authorization, $user_agent, $property_id, $room_id, $rate_id, $token, $customer_ip, $customer_session_id, $test);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShoppingApi->propertiesPropertyIdRoomsRoomIdRatesRateIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **property_id** | **string**| Expedia Property ID.&lt;br&gt; Example: &#x60;19248&#x60; |
 **room_id** | **string**| Room ID of a property.&lt;br&gt; Example: &#x60;123abc&#x60;. |
 **rate_id** | **string**| Rate ID of a room.&lt;br&gt; Example: &#x60;123abc&#x60; |
 **token** | **string**| A hashed collection of query parameters. Used to maintain state across calls. This token is provided as part of the price check link from the shop response. |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. | [optional]
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| Price check calls have a test header that can be used to return set responses with the following keywords:   * &#x60;available&#x60;   * &#x60;price_changed&#x60;   * &#x60;sold_out&#x60;   * &#x60;service_unavailable&#x60;   * &#x60;unknown_internal_error&#x60; | [optional]

### Return type

[**\Swagger\Client\Model\RoomPriceCheck**](../Model/RoomPriceCheck.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

