# Swagger\Client\GeographyApi

All URIs are relative to *https://api.ean.com/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**propertiesGeographyPost**](GeographyApi.md#propertiesGeographyPost) | **POST** /properties/geography | Properties within Polygon
[**regionsGet**](GeographyApi.md#regionsGet) | **GET** /regions | Regions
[**regionsRegionIdGet**](GeographyApi.md#regionsRegionIdGet) | **GET** /regions/{region_id} | Region


# **propertiesGeographyPost**
> map[string,\Swagger\Client\Model\PropertyGeography] propertiesGeographyPost($accept, $accept_encoding, $authorization, $content_type, $user_agent, $include, $properties_geography_request_body, $customer_session_id, $billing_terms, $partner_point_of_sale, $payment_terms, $platform_name)

Properties within Polygon

Returns the properties within an custom polygon that represents a multi-city area or smaller. The coordinates of the polygon should be in [GeoJSON format](https://tools.ietf.org/html/rfc7946) and the polygon must conform to the following restrictions:   * Polygon size - diagonal distance of the polygon must be less than 500km   * Polygon type - only single polygons are supported   * Number of coordinates - must be <= 2000

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GeographyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$content_type = "content_type_example"; // string | This parameter is to specify what format the request body is in. The only supported value is application/json.  Example: `application/json`
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$include = "include_example"; // string | Options for which content to return in the response. The value must be lower case.   * property_ids - Include the property IDs.  Example: `property_ids` Possible values: `property_ids`.
$properties_geography_request_body = new \Swagger\Client\Model\PropertiesGeoJsonRequest(); // \Swagger\Client\Model\PropertiesGeoJsonRequest | 
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$billing_terms = "billing_terms_example"; // string | This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately.
$partner_point_of_sale = "partner_point_of_sale_example"; // string | This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately.
$payment_terms = "payment_terms_example"; // string | This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately.
$platform_name = "platform_name_example"; // string | This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately.

try {
    $result = $apiInstance->propertiesGeographyPost($accept, $accept_encoding, $authorization, $content_type, $user_agent, $include, $properties_geography_request_body, $customer_session_id, $billing_terms, $partner_point_of_sale, $payment_terms, $platform_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeographyApi->propertiesGeographyPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **content_type** | **string**| This parameter is to specify what format the request body is in. The only supported value is application/json.  Example: &#x60;application/json&#x60; |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **include** | **string**| Options for which content to return in the response. The value must be lower case.   * property_ids - Include the property IDs.  Example: &#x60;property_ids&#x60; Possible values: &#x60;property_ids&#x60;. |
 **properties_geography_request_body** | [**\Swagger\Client\Model\PropertiesGeoJsonRequest**](../Model/PropertiesGeoJsonRequest.md)|  |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **billing_terms** | **string**| This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately. | [optional]
 **partner_point_of_sale** | **string**| This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]
 **payment_terms** | **string**| This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately. | [optional]
 **platform_name** | **string**| This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]

### Return type

[**map[string,\Swagger\Client\Model\PropertyGeography]**](../Model/PropertyGeography.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **regionsGet**
> \Swagger\Client\Model\Region[] regionsGet($accept, $accept_encoding, $authorization, $user_agent, $language, $include, $customer_session_id, $ancestor_id, $iata_location_code, $billing_terms, $partner_point_of_sale, $payment_terms, $platform_name)

Regions

Returns the geographic definition and property mappings of regions matching the specified parameters.<br><br>  To request all regions in the world, omit the `ancestor` query parameter. To request all regions in a specific continent, country or other level, specify the ID of that region as the `ancestor`. Refer to the list of [top level regions](https://developer.expediapartnersolutions.com/reference/geography-reference-lists-v3/).<br><br>  The response is a paginated list of regions. The response will contain a header, `Link`. The `Link` header contains a single URL to get the immediate next page of results, and follows the [IETF standard](https://tools.ietf.org/html/rfc5988). To get the next page of results, simply follow the `next` URL in the `Link` header without modifying it. When no `Link` header is returned with an empty body and a 200 response code, the pagination has completed. If the link expires, there will be an `expires` link-extension that is the UTC date the link will expire, in ISO 8601 format.<br>  * Example: `<https://api.ean.com/v3/regions?token=DXF1ZXJ5QW5kRmV0Y2gBAAAAAAdcoBgWbUpHYTdsdFVRc2U4c0xfLUhGMzM1QQ>; rel=\"next\"; expires=\"2019-03-05T07:23:14.000Z\"`

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GeographyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$language = "language_example"; // string | Desired language for the response as a subset of BCP47 format that only uses hyphenated pairs of two-digit language and country codes. Use only ISO639-1 alpha 2 language codes and ISO3166-1 alpha 2 country codes. See [https://www.w3.org/International/articles/language-tags/](https://www.w3.org/International/articles/language-tags/)  Example: `language=en-US`  Language Options: [https://developer.expediapartnersolutions.com/reference/language-options/](https://developer.expediapartnersolutions.com/reference/language-options/)
$include = array("include_example"); // string[] | Options for which content to return in the response. This parameter can be supplied multiple times with different values. The standard and details options cannot be requested together. The value must be lower case.   * standard - Include the metadata and basic hierarchy of each region.   * details - Include the metadata, coordinates and full hierarchy of each region.   * property_ids - Include the list of property IDs within the bounding polygon of each region.   * property_ids_expanded - Include the list of property IDs within the bounding polygon of each region and property IDs from the surrounding area if minimal properties are within the region.  Example: `include=details&include=property_ids`
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$ancestor_id = "ancestor_id_example"; // string | Search for regions whose ancestors include the requested ancestor region ID. Refer to the list of [top level regions](https://developer.expediapartnersolutions.com/reference/geography-reference-lists-v3/).  Example: `ancestor_id=602962`.
$iata_location_code = "iata_location_code_example"; // string | Search for regions by the requested 3-character IATA location code, which will apply to both iata_airport_code and iata_airport_metro_code. The code must be upper case.  Example: `iata_location_code=ORD`.
$billing_terms = "billing_terms_example"; // string | This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately.
$partner_point_of_sale = "partner_point_of_sale_example"; // string | This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately.
$payment_terms = "payment_terms_example"; // string | This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately.
$platform_name = "platform_name_example"; // string | This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately.

try {
    $result = $apiInstance->regionsGet($accept, $accept_encoding, $authorization, $user_agent, $language, $include, $customer_session_id, $ancestor_id, $iata_location_code, $billing_terms, $partner_point_of_sale, $payment_terms, $platform_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeographyApi->regionsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **language** | **string**| Desired language for the response as a subset of BCP47 format that only uses hyphenated pairs of two-digit language and country codes. Use only ISO639-1 alpha 2 language codes and ISO3166-1 alpha 2 country codes. See [https://www.w3.org/International/articles/language-tags/](https://www.w3.org/International/articles/language-tags/)  Example: &#x60;language&#x3D;en-US&#x60;  Language Options: [https://developer.expediapartnersolutions.com/reference/language-options/](https://developer.expediapartnersolutions.com/reference/language-options/) |
 **include** | [**string[]**](../Model/string.md)| Options for which content to return in the response. This parameter can be supplied multiple times with different values. The standard and details options cannot be requested together. The value must be lower case.   * standard - Include the metadata and basic hierarchy of each region.   * details - Include the metadata, coordinates and full hierarchy of each region.   * property_ids - Include the list of property IDs within the bounding polygon of each region.   * property_ids_expanded - Include the list of property IDs within the bounding polygon of each region and property IDs from the surrounding area if minimal properties are within the region.  Example: &#x60;include&#x3D;details&amp;include&#x3D;property_ids&#x60; |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **ancestor_id** | **string**| Search for regions whose ancestors include the requested ancestor region ID. Refer to the list of [top level regions](https://developer.expediapartnersolutions.com/reference/geography-reference-lists-v3/).  Example: &#x60;ancestor_id&#x3D;602962&#x60;. | [optional]
 **iata_location_code** | **string**| Search for regions by the requested 3-character IATA location code, which will apply to both iata_airport_code and iata_airport_metro_code. The code must be upper case.  Example: &#x60;iata_location_code&#x3D;ORD&#x60;. | [optional]
 **billing_terms** | **string**| This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately. | [optional]
 **partner_point_of_sale** | **string**| This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]
 **payment_terms** | **string**| This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately. | [optional]
 **platform_name** | **string**| This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]

### Return type

[**\Swagger\Client\Model\Region[]**](../Model/Region.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **regionsRegionIdGet**
> \Swagger\Client\Model\Region regionsRegionIdGet($accept, $accept_encoding, $authorization, $user_agent, $region_id, $language, $include, $customer_session_id, $billing_terms, $partner_point_of_sale, $payment_terms, $platform_name)

Region

Returns the geographic definition and property mappings for the requested Region ID. The response is a single JSON formatted region object.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\GeographyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$region_id = "region_id_example"; // string | ID of the region to retrieve.  Example: `178248`.
$language = "language_example"; // string | Desired language for the response as a subset of BCP47 format that only uses hyphenated pairs of two-digit language and country codes. Use only ISO639-1 alpha 2 language codes and ISO3166-1 alpha 2 country codes. See [https://www.w3.org/International/articles/language-tags/](https://www.w3.org/International/articles/language-tags/)  Example: `language=en-US`  Language Options: [https://developer.expediapartnersolutions.com/reference/language-options/](https://developer.expediapartnersolutions.com/reference/language-options/)
$include = array("include_example"); // string[] | Options for which content to return in the response. This parameter can be supplied multiple times with different values. The value must be lower case.   * details - Include the metadata, coordinates and full hierarchy of the region.   * property_ids - Include the list of property IDs within the bounding polygon of the region.   * property_ids_expanded - Include the list of property IDs within the bounding polygon of the region and property IDs from the surrounding area if minimal properties are within the region.  Example: `include=details&include=property_ids`
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$billing_terms = "billing_terms_example"; // string | This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately.
$partner_point_of_sale = "partner_point_of_sale_example"; // string | This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately.
$payment_terms = "payment_terms_example"; // string | This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately.
$platform_name = "platform_name_example"; // string | This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately.

try {
    $result = $apiInstance->regionsRegionIdGet($accept, $accept_encoding, $authorization, $user_agent, $region_id, $language, $include, $customer_session_id, $billing_terms, $partner_point_of_sale, $payment_terms, $platform_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeographyApi->regionsRegionIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **region_id** | **string**| ID of the region to retrieve.  Example: &#x60;178248&#x60;. |
 **language** | **string**| Desired language for the response as a subset of BCP47 format that only uses hyphenated pairs of two-digit language and country codes. Use only ISO639-1 alpha 2 language codes and ISO3166-1 alpha 2 country codes. See [https://www.w3.org/International/articles/language-tags/](https://www.w3.org/International/articles/language-tags/)  Example: &#x60;language&#x3D;en-US&#x60;  Language Options: [https://developer.expediapartnersolutions.com/reference/language-options/](https://developer.expediapartnersolutions.com/reference/language-options/) |
 **include** | [**string[]**](../Model/string.md)| Options for which content to return in the response. This parameter can be supplied multiple times with different values. The value must be lower case.   * details - Include the metadata, coordinates and full hierarchy of the region.   * property_ids - Include the list of property IDs within the bounding polygon of the region.   * property_ids_expanded - Include the list of property IDs within the bounding polygon of the region and property IDs from the surrounding area if minimal properties are within the region.  Example: &#x60;include&#x3D;details&amp;include&#x3D;property_ids&#x60; |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **billing_terms** | **string**| This parameter is to specify the terms of how a resulting booking should be billed. If this field is needed, the value for this will be provided to you separately. | [optional]
 **partner_point_of_sale** | **string**| This parameter is to specify what point of sale is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]
 **payment_terms** | **string**| This parameter is to specify what terms should be used when being paid for a resulting booking. If this field is needed, the value for this will be provided to you separately. | [optional]
 **platform_name** | **string**| This parameter is to specify what platform is being used to shop and book. If this field is needed, the value for this will be provided to you separately. | [optional]

### Return type

[**\Swagger\Client\Model\Region**](../Model/Region.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

