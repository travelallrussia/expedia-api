# Swagger\Client\AuthenticationApi

All URIs are relative to *https://api.ean.com/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**oauthTokenPost**](AuthenticationApi.md#oauthTokenPost) | **POST** /oauth/token | OAuth2 Authentication


# **oauthTokenPost**
> \Swagger\Client\Model\InlineResponse200 oauthTokenPost($accept, $accept_encoding, $content_type, $client_id, $client_secret)

OAuth2 Authentication

<em>This feature is reserved as a future optional authentication method.</em></br></br> OAuth2 authentication allows you to authenticate periodically rather than sending your credentials with every request. Once you have authenticated, you will receive an access token. The access token can be used on subsequent API calls to maintain access until it expires.</br></br>  When the access token expires, you will receive an error letting you know that it has expired. To avoid seeing this error, we recommend you retrieve a new token several seconds before the token expires. The `expires_in` attribute in the response indicates the number of seconds a token is valid.</br></br>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$content_type = "content_type_example"; // string | This parameter is to specify what format the request body is in. The only supported value is application/x-www-form-urlencoded.  Example: `application/x-www-form-urlencoded`
$client_id = "client_id_example"; // string | 
$client_secret = "client_secret_example"; // string | 

try {
    $result = $apiInstance->oauthTokenPost($accept, $accept_encoding, $content_type, $client_id, $client_secret);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->oauthTokenPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **content_type** | **string**| This parameter is to specify what format the request body is in. The only supported value is application/x-www-form-urlencoded.  Example: &#x60;application/x-www-form-urlencoded&#x60; |
 **client_id** | **string**|  |
 **client_secret** | **string**|  |

### Return type

[**\Swagger\Client\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

