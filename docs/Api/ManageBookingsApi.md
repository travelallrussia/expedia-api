# Swagger\Client\ManageBookingsApi

All URIs are relative to *https://api.ean.com/v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**itinerariesGet**](ManageBookingsApi.md#itinerariesGet) | **GET** /itineraries | Search for and retrieve Bookings with Affiliate Reference Id
[**itinerariesItineraryIdDelete**](ManageBookingsApi.md#itinerariesItineraryIdDelete) | **DELETE** /itineraries/{itinerary_id} | Cancel Held Booking
[**itinerariesItineraryIdGet**](ManageBookingsApi.md#itinerariesItineraryIdGet) | **GET** /itineraries/{itinerary_id} | Retrieve Booking
[**itinerariesItineraryIdRoomsRoomIdDelete**](ManageBookingsApi.md#itinerariesItineraryIdRoomsRoomIdDelete) | **DELETE** /itineraries/{itinerary_id}/rooms/{room_id} | Cancel a room.
[**itinerariesItineraryIdRoomsRoomIdPut**](ManageBookingsApi.md#itinerariesItineraryIdRoomsRoomIdPut) | **PUT** /itineraries/{itinerary_id}/rooms/{room_id} | Change details of a room.


# **itinerariesGet**
> \Swagger\Client\Model\Itinerary[] itinerariesGet($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $affiliate_reference_id, $email, $customer_session_id, $test)

Search for and retrieve Bookings with Affiliate Reference Id

This can be called directly without a token when an affiliate reference id is provided. It returns details about bookings associated with an affiliate reference id, along with cancel links to cancel the bookings.  <i>Note: Newly created itineraries may sometimes have a small delay between the time of creation and the time that the itinerary can be retrieved. If you receive no results while trying to find an itinerary that was successfully created, please wait a few minutes before trying to serarch for the itinerary again.</i>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ManageBookingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$affiliate_reference_id = "affiliate_reference_id_example"; // string | The affilliate reference id value. This field supports a maximum of 28 characters. Example: `111A222BB33344CC5555`
$email = "email_example"; // string | Email associated with the booking.<br> Example: `test@example.com`.
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | The retrieve call has a test header that can be used to return set responses with the following keywords:<br> * `standard` - Requires valid test booking. * `service_unavailable` * `internal_server_error`

try {
    $result = $apiInstance->itinerariesGet($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $affiliate_reference_id, $email, $customer_session_id, $test);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManageBookingsApi->itinerariesGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **affiliate_reference_id** | **string**| The affilliate reference id value. This field supports a maximum of 28 characters. Example: &#x60;111A222BB33344CC5555&#x60; |
 **email** | **string**| Email associated with the booking.&lt;br&gt; Example: &#x60;test@example.com&#x60;. |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| The retrieve call has a test header that can be used to return set responses with the following keywords:&lt;br&gt; * &#x60;standard&#x60; - Requires valid test booking. * &#x60;service_unavailable&#x60; * &#x60;internal_server_error&#x60; | [optional]

### Return type

[**\Swagger\Client\Model\Itinerary[]**](../Model/Itinerary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **itinerariesItineraryIdDelete**
> itinerariesItineraryIdDelete($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $itinerary_id, $token, $customer_session_id, $test)

Cancel Held Booking

This link will be available in a held booking response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ManageBookingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$itinerary_id = "itinerary_id_example"; // string | This parameter is used only to prefix the token value - no ID value is used.<br> Example: `8955599932111`
$token = "token_example"; // string | Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: `MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m`
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | The cancel call has a test header that can be used to return set responses with the following keywords:<br> * `standard` - Requires valid test held booking. * `service_unavailable` * `internal_server_error` * `post_stay_cancel`

try {
    $apiInstance->itinerariesItineraryIdDelete($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $itinerary_id, $token, $customer_session_id, $test);
} catch (Exception $e) {
    echo 'Exception when calling ManageBookingsApi->itinerariesItineraryIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **itinerary_id** | **string**| This parameter is used only to prefix the token value - no ID value is used.&lt;br&gt; Example: &#x60;8955599932111&#x60; |
 **token** | **string**| Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: &#x60;MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m&#x60; |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| The cancel call has a test header that can be used to return set responses with the following keywords:&lt;br&gt; * &#x60;standard&#x60; - Requires valid test held booking. * &#x60;service_unavailable&#x60; * &#x60;internal_server_error&#x60; * &#x60;post_stay_cancel&#x60; | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **itinerariesItineraryIdGet**
> \Swagger\Client\Model\Itinerary itinerariesItineraryIdGet($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $itinerary_id, $customer_session_id, $test, $token, $email)

Retrieve Booking

This API call returns itinerary details and links to resume or cancel the booking. There are two methods to retrieve a booking: * Using the link included in the original Book response, example: https://api.ean.com/v3/itineraries/8955599932111?token=QldfCGlcUA4GXVlSAQ4W * Using the email of the booking. If the email contains special characters, they must be encoded to successfully retrieve the booking. Example: https://api.ean.com/v3/itineraries/8955599932111?email=customer@email.com  <i>Note: Newly created itineraries may sometimes have a small delay between the time of creation and the time that the itinerary can be retrieved. If you receive an error when trying to retrieve an itinerary that was successfully created, please wait a few minutes before trying to retrieve the itinerary again.</i>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ManageBookingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$itinerary_id = "itinerary_id_example"; // string | This parameter is used only to prefix the token value - no ID value is used.<br> Example: `8955599932111`
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | The retrieve call has a test header that can be used to return set responses. Passing standard in the Test header will retrieve a test booking, and passing any of the errors listed below will return a stubbed error response that you can use to test your error handling code. Additionally, refer to the Test Request documentation for more details on how these header values are used. * `standard` - Requires valid test booking. * `service_unavailable` * `internal_server_error`
$token = "token_example"; // string | Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: `MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m`
$email = "email_example"; // string | Email associated with the booking. (Email is required if the token is not provided the request) <br> Example: `test@example.com`.

try {
    $result = $apiInstance->itinerariesItineraryIdGet($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $itinerary_id, $customer_session_id, $test, $token, $email);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ManageBookingsApi->itinerariesItineraryIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **itinerary_id** | **string**| This parameter is used only to prefix the token value - no ID value is used.&lt;br&gt; Example: &#x60;8955599932111&#x60; |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| The retrieve call has a test header that can be used to return set responses. Passing standard in the Test header will retrieve a test booking, and passing any of the errors listed below will return a stubbed error response that you can use to test your error handling code. Additionally, refer to the Test Request documentation for more details on how these header values are used. * &#x60;standard&#x60; - Requires valid test booking. * &#x60;service_unavailable&#x60; * &#x60;internal_server_error&#x60; | [optional]
 **token** | **string**| Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: &#x60;MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m&#x60; | [optional]
 **email** | **string**| Email associated with the booking. (Email is required if the token is not provided the request) &lt;br&gt; Example: &#x60;test@example.com&#x60;. | [optional]

### Return type

[**\Swagger\Client\Model\Itinerary**](../Model/Itinerary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **itinerariesItineraryIdRoomsRoomIdDelete**
> itinerariesItineraryIdRoomsRoomIdDelete($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $itinerary_id, $room_id, $token, $customer_session_id, $test)

Cancel a room.

This link will be available in the retrieve response.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ManageBookingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$itinerary_id = "itinerary_id_example"; // string | This parameter is used only to prefix the token value - no ID value is used.<br> Example: `8955599932111`
$room_id = "room_id_example"; // string | Room ID of a property.<br> Example: `123abc`.
$token = "token_example"; // string | Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: `MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m`
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | The cancel call has a test header that can be used to return set responses with the following keywords:<br> * `standard` - Requires valid test booking. * `service_unavailable` * `unknown_internal_error` * `post_stay_cancel`

try {
    $apiInstance->itinerariesItineraryIdRoomsRoomIdDelete($accept, $accept_encoding, $authorization, $customer_ip, $user_agent, $itinerary_id, $room_id, $token, $customer_session_id, $test);
} catch (Exception $e) {
    echo 'Exception when calling ManageBookingsApi->itinerariesItineraryIdRoomsRoomIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **itinerary_id** | **string**| This parameter is used only to prefix the token value - no ID value is used.&lt;br&gt; Example: &#x60;8955599932111&#x60; |
 **room_id** | **string**| Room ID of a property.&lt;br&gt; Example: &#x60;123abc&#x60;. |
 **token** | **string**| Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: &#x60;MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m&#x60; |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| The cancel call has a test header that can be used to return set responses with the following keywords:&lt;br&gt; * &#x60;standard&#x60; - Requires valid test booking. * &#x60;service_unavailable&#x60; * &#x60;unknown_internal_error&#x60; * &#x60;post_stay_cancel&#x60; | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **itinerariesItineraryIdRoomsRoomIdPut**
> itinerariesItineraryIdRoomsRoomIdPut($accept, $accept_encoding, $authorization, $content_type, $customer_ip, $user_agent, $itinerary_id, $room_id, $token, $room_details_request_body, $customer_session_id, $test)

Change details of a room.

This link will be available in the retrieve response. Changes in smoking preference and special request will be passed along to the property and are not guaranteed.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ManageBookingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept = "accept_example"; // string | Specifies the response format that the client would like to receive back. This must be `application/json`.  Example: `application/json`
$accept_encoding = "accept_encoding_example"; // string | Specifies the response encoding that the client would like to receive back. This must be `gzip`.  Example: `gzip`
$authorization = "authorization_example"; // string | The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details.
$content_type = "content_type_example"; // string | This parameter is to specify what format the request body is in. The only supported value is application/json.  Example: `application/json`
$customer_ip = "customer_ip_example"; // string | IP address of the customer, as captured by your integration. Send IPV4 addresses only.<br> Ensure your integration passes the customer's IP, not your own. This value helps determine their location and assign the correct payment gateway.<br> Also used for fraud recovery and other important analytics.
$user_agent = "user_agent_example"; // string | The `User-Agent` header string from the customer's request, as captured by your integration. If you are building an application then the `User-Agent` value should be `{app name}/{app version}`.  Example: `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36`  Example: `TravelNow/3.30.112`
$itinerary_id = "itinerary_id_example"; // string | This parameter is used only to prefix the token value - no ID value is used.<br> Example: `8955599932111`
$room_id = "room_id_example"; // string | Room ID of a property.<br> Example: `123abc`.
$token = "token_example"; // string | Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: `MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m`
$room_details_request_body = new \Swagger\Client\Model\ChangeRoomDetailsRequest(); // \Swagger\Client\Model\ChangeRoomDetailsRequest | The request body is required, but only the fields that are being changed need to be passed in. Fields that are not being changed should not be included in the request body.
$customer_session_id = "customer_session_id_example"; // string | Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user's session, using a new value for every new customer session.<br> Including this value greatly eases EPS's internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user's session.
$test = "test_example"; // string | The change call has a test header that can be used to return set responses with the following keywords:<br> * `standard` - Requires valid test booking. * `service_unavailable` * `unknown_internal_error`

try {
    $apiInstance->itinerariesItineraryIdRoomsRoomIdPut($accept, $accept_encoding, $authorization, $content_type, $customer_ip, $user_agent, $itinerary_id, $room_id, $token, $room_details_request_body, $customer_session_id, $test);
} catch (Exception $e) {
    echo 'Exception when calling ManageBookingsApi->itinerariesItineraryIdRoomsRoomIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept** | **string**| Specifies the response format that the client would like to receive back. This must be &#x60;application/json&#x60;.  Example: &#x60;application/json&#x60; |
 **accept_encoding** | **string**| Specifies the response encoding that the client would like to receive back. This must be &#x60;gzip&#x60;.  Example: &#x60;gzip&#x60; |
 **authorization** | **string**| The custom generated authentication header. Refer to our [signature authentication](https://developer.expediapartnersolutions.com/reference/signature-authentication) page for full details. |
 **content_type** | **string**| This parameter is to specify what format the request body is in. The only supported value is application/json.  Example: &#x60;application/json&#x60; |
 **customer_ip** | **string**| IP address of the customer, as captured by your integration. Send IPV4 addresses only.&lt;br&gt; Ensure your integration passes the customer&#39;s IP, not your own. This value helps determine their location and assign the correct payment gateway.&lt;br&gt; Also used for fraud recovery and other important analytics. |
 **user_agent** | **string**| The &#x60;User-Agent&#x60; header string from the customer&#39;s request, as captured by your integration. If you are building an application then the &#x60;User-Agent&#x60; value should be &#x60;{app name}/{app version}&#x60;.  Example: &#x60;Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36&#x60;  Example: &#x60;TravelNow/3.30.112&#x60; |
 **itinerary_id** | **string**| This parameter is used only to prefix the token value - no ID value is used.&lt;br&gt; Example: &#x60;8955599932111&#x60; |
 **room_id** | **string**| Room ID of a property.&lt;br&gt; Example: &#x60;123abc&#x60;. |
 **token** | **string**| Provided as part of the link object and used to maintain state across calls. This simplifies each subsequent call by limiting the amount of information required at each step and reduces the potential for errors. Token values cannot be viewed or changed. Example: &#x60;MY5S3j36cOcLfLBZjPYQ1abhfc8CqmjmFVzkk7euvWaunE57LLeDgaxm516m&#x60; |
 **room_details_request_body** | [**\Swagger\Client\Model\ChangeRoomDetailsRequest**](../Model/ChangeRoomDetailsRequest.md)| The request body is required, but only the fields that are being changed need to be passed in. Fields that are not being changed should not be included in the request body. |
 **customer_session_id** | **string**| Insert your own unique value for each user session, beginning with the first API call. Continue to pass the same value for each subsequent API call during the user&#39;s session, using a new value for every new customer session.&lt;br&gt; Including this value greatly eases EPS&#39;s internal debugging process for issues with partner requests, as it explicitly links together request paths for individual user&#39;s session. | [optional]
 **test** | **string**| The change call has a test header that can be used to return set responses with the following keywords:&lt;br&gt; * &#x60;standard&#x60; - Requires valid test booking. * &#x60;service_unavailable&#x60; * &#x60;unknown_internal_error&#x60; | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

