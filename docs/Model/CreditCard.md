# CreditCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_options** | [**\Swagger\Client\Model\CardOption[]**](CardOption.md) |  | [optional] 
**merchant** | [**\Swagger\Client\Model\CreditCardMerchant**](CreditCardMerchant.md) |  | [optional] 
**name** | **string** | Display name of payment option. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


