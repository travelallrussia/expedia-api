# BedGroupAvailability

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to confirm pricing and availability for the selected rate. | [optional] 
**id** | **string** | Unique identifier for a bed group. | [optional] 
**description** | **string** | This is a display ready description of a bed configuration for this room. | [optional] 
**configuration** | [**\Swagger\Client\Model\BedGroupConfiguration[]**](BedGroupConfiguration.md) | The bed configuration for a given room. This array can be empty for the related bed group. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


