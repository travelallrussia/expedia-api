# PropertyAvailability

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_id** | **string** | Expedia property ID. | [optional] 
**rooms** | [**\Swagger\Client\Model\RoomAvailability[]**](RoomAvailability.md) | Array of objects containing room information. | [optional] 
**score** | **float** | A score to sort properties where the higher the value the better. It can be used to:&lt;br&gt; * Sort the properties on the response&lt;br&gt; * Sort properties across multiple responses in parallel searches for large regions&lt;br&gt; | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to request additional rates and recommendations. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


