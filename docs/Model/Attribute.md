# Attribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The attribute definition ID for this attribute. | [optional] 
**name** | **string** | Attribute name. | [optional] 
**value** | **string** | Attribute value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


