# PromotionsItinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value_adds** | [**map[string,\Swagger\Client\Model\ValueAdd]**](ValueAdd.md) | Promotions provided by the property that add value to the stay, but don’t affect the booking price (i.e., ski lift tickets or premium wifi). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


