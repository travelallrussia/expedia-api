# ItineraryCreation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itinerary_id** | **string** | The itinerary id. | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to retrieve a booking, resume a held booking, cancel a held booking, or complete a payment session if a challenge is used. | [optional] 
**encoded_challenge_config** | **string** | The challenge config that is required to perform payment challenge. This field will be available when payment challenge is needed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


