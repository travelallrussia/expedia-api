# AmenityDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier for the amenity definition. | [optional] 
**name** | **string** | Amenity definition name. | [optional] 
**type** | **string** | Amenity definition type. | [optional] 
**has_value** | **bool** | Boolean to indicate if the amenity will have a value. | [optional] 
**categories** | [**\Swagger\Client\Model\CategoryAmenity[]**](CategoryAmenity.md) | Categories the amenity will belong to. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


