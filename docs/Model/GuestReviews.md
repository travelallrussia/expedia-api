# GuestReviews

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verified** | [**\Swagger\Client\Model\Review[]**](Review.md) | A collection of the guest reviews which have been verified, in order, starting with the newest. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


