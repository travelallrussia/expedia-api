# BillingContactRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **string** | First/given name of the payment type account holder. | 
**family_name** | **string** | Last/family name of the payment type account holder. | 
**address** | [**\Swagger\Client\Model\BillingContactRequestAddress**](BillingContactRequestAddress.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


