# Ratings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property** | [**\Swagger\Client\Model\PropertyRating**](PropertyRating.md) |  | [optional] 
**guest** | [**\Swagger\Client\Model\GuestRating**](GuestRating.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


