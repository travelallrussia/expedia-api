# CoordinatesRegion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**center_longitude** | **float** | Center Longitude. | [optional] 
**center_latitude** | **float** | Center Latitude. | [optional] 
**bounding_polygon** | [**\Swagger\Client\Model\BoundingPolygon**](BoundingPolygon.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


