# BoundingPolygon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Type of bounding polygon. | [optional] 
**coordinates** | [**float[][][][]**](array.md) | An array of multiple polygon(s) that combine to make a full MultiPolygon in geojson format. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


