# PropertyContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_id** | **string** | Unique Expedia property ID. | [optional] 
**name** | **string** | Property name. | [optional] 
**address** | [**\Swagger\Client\Model\Address**](Address.md) |  | [optional] 
**ratings** | [**\Swagger\Client\Model\Ratings**](Ratings.md) |  | [optional] 
**location** | [**\Swagger\Client\Model\Location**](Location.md) |  | [optional] 
**phone** | **string** | The property&#39;s phone number. | [optional] 
**fax** | **string** | The property&#39;s fax number. | [optional] 
**category** | [**\Swagger\Client\Model\CategoryProperty**](CategoryProperty.md) |  | [optional] 
**business_model** | [**\Swagger\Client\Model\BusinessModel**](BusinessModel.md) |  | [optional] 
**rank** | **float** | The property’s rank across all properties. This value sorts properties based on EPS transactional data and details about the property, with 1 indicating the best-performing property and others following in ascending numerical order. | [optional] 
**checkin** | [**\Swagger\Client\Model\Checkin**](Checkin.md) |  | [optional] 
**checkout** | [**\Swagger\Client\Model\Checkout**](Checkout.md) |  | [optional] 
**fees** | [**\Swagger\Client\Model\Fees**](Fees.md) |  | [optional] 
**policies** | [**\Swagger\Client\Model\Policies**](Policies.md) |  | [optional] 
**attributes** | [**\Swagger\Client\Model\Attributes**](Attributes.md) |  | [optional] 
**amenities** | [**map[string,\Swagger\Client\Model\Amenity]**](Amenity.md) | Lists all of the amenities available for all guests at the property. See our [amenities reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_amenity_definitions) for current known amenity ID and name values. | [optional] 
**images** | [**\Swagger\Client\Model\Image[]**](Image.md) | Contains all property images available. | [optional] 
**onsite_payments** | [**\Swagger\Client\Model\OnsitePayments**](OnsitePayments.md) |  | [optional] 
**rooms** | [**map[string,\Swagger\Client\Model\RoomContent]**](RoomContent.md) | Information about all of the rooms at the property. | [optional] 
**rates** | [**map[string,\Swagger\Client\Model\RateContent]**](RateContent.md) | Additional information about the rates offered by the property. This should be used in conjunction with the pricing and other rate-related information in shopping. | [optional] 
**dates** | [**\Swagger\Client\Model\Dates**](Dates.md) |  | [optional] 
**descriptions** | [**\Swagger\Client\Model\Descriptions**](Descriptions.md) |  | [optional] 
**statistics** | [**map[string,\Swagger\Client\Model\Statistic]**](Statistic.md) | Statistics of the property, such as number of floors. See our [statistics reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_statistic_definitions) for current known statistics ID and name values. | [optional] 
**airports** | [**\Swagger\Client\Model\AssociatedAirports**](AssociatedAirports.md) |  | [optional] 
**themes** | [**map[string,\Swagger\Client\Model\Theme]**](Theme.md) | Themes that describe the property. See our [themes reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_themes) for current known theme ID and name values. | [optional] 
**all_inclusive** | [**\Swagger\Client\Model\AllInclusive**](AllInclusive.md) |  | [optional] 
**tax_id** | **string** | Tax ID. | [optional] 
**chain** | [**\Swagger\Client\Model\Chain**](Chain.md) |  | [optional] 
**brand** | [**\Swagger\Client\Model\Brand**](Brand.md) |  | [optional] 
**spoken_languages** | [**map[string,\Swagger\Client\Model\SpokenLanguage]**](SpokenLanguage.md) | Languages spoken at the property. | [optional] 
**multi_unit** | **bool** | Boolean value indicating if a property is a multi-unit property. | [optional] 
**payment_registration_recommended** | **bool** | Boolean value indicating if a property may require payment registration to process payments, even when using the property_collect Business Model. If true, then a property may not be successfully bookable without registering payments first. | [optional] 
**vacation_rental_details** | [**\Swagger\Client\Model\VacationRentalDetails**](VacationRentalDetails.md) |  | [optional] 
**supply_source** | **string** | The supply source of the property. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


