# CreateItineraryRequestRooms

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **string** | First name of room guest. | 
**family_name** | **string** | Last name of room guest. | 
**smoking** | **bool** | Specify if the guest would prefer a smoking room. This field is only a request and the property is not guaranteed to honor it, it will not override any non-smoking policies by the hotel. Defaults to false. | 
**special_request** | **string** | Special requests to send to hotel (not guaranteed). Do not use this field to communicate B2B customer service requests or pass any sensitive personal or financial information (PII). | [optional] 
**loyalty_id** | **string** | A loyalty identifier for a hotel loyalty program associated with this room guest. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


