# Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | **string** | The request method used to access the link. | [optional] 
**href** | **string** | The URL for the link. This can be absolute or relative. | [optional] 
**expires** | **string** | If the link expires, this will be the UTC date the link will expire, in ISO 8601 format. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


