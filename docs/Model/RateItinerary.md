# RateItinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The id of the rate. | [optional] 
**merchant_of_record** | [**\Swagger\Client\Model\MerchantOfRecord**](MerchantOfRecord.md) |  | [optional] 
**refundable** | **bool** | Indicates whether the itinerary is refundable or not. | [optional] 
**cancel_refund** | [**\Swagger\Client\Model\CancelRefund**](CancelRefund.md) |  | [optional] 
**amenities** | **string[]** |  | [optional] 
**promotions** | [**\Swagger\Client\Model\PromotionsItinerary**](PromotionsItinerary.md) |  | [optional] 
**cancel_penalties** | [**\Swagger\Client\Model\CancelPenaltyItinerary[]**](CancelPenaltyItinerary.md) |  | [optional] 
**nonrefundable_date_ranges** | [**\Swagger\Client\Model\NonrefundableDateRange[]**](NonrefundableDateRange.md) | A list of date exceptions. Dates within these ranges provide no refund on cancellation, regardless of cancel penalty windows. | [optional] 
**deposits** | [**\Swagger\Client\Model\DepositItinerary[]**](DepositItinerary.md) |  | [optional] 
**card_on_file_limit** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**refundable_damage_deposit** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**pricing** | [**\Swagger\Client\Model\PricingInformation**](PricingInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


