# BedGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique identifier for a bed group. | [optional] 
**description** | **string** | This is a display ready description of a bed configuration for this room. | [optional] 
**configuration** | [**\Swagger\Client\Model\BedGroupConfiguration[]**](BedGroupConfiguration.md) | An array of bed configurations for this room. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


