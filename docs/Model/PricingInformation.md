# PricingInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nightly** | [**\Swagger\Client\Model\NightCharge[][]**](array.md) | Array of arrays of amount objects. Each sub-array of amount objects represents a single night&#39;s charges. | [optional] 
**stay** | [**\Swagger\Client\Model\Stay[]**](Stay.md) | Array of amount objects. Details any charges that apply to the entire stay (not divided per-night). Any per-room adjustments are applied to the &#x60;base_rate&#x60; amount within this object. | [optional] 
**totals** | [**\Swagger\Client\Model\Totals**](Totals.md) |  | [optional] 
**fees** | [**\Swagger\Client\Model\FeesPricingInformation**](FeesPricingInformation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


