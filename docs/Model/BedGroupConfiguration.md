# BedGroupConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The type of this bed configuration in the room. | [optional] 
**size** | **string** | The size of this bed configuration in the room. | [optional] 
**quantity** | **float** | The number of this bed configuration in the room. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


