# RoomContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique identifier for a room type. | [optional] 
**name** | **string** | Room type name. | [optional] 
**descriptions** | [**\Swagger\Client\Model\DescriptionsRoom**](DescriptionsRoom.md) |  | [optional] 
**amenities** | [**map[string,\Swagger\Client\Model\Amenity]**](Amenity.md) | Lists all of the amenities available in the room. See our [amenities reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_amenity_definitions) for current known amenity ID and name values. | [optional] 
**images** | [**\Swagger\Client\Model\Image[]**](Image.md) | The room&#39;s images. Contains all room images available. | [optional] 
**bed_groups** | [**map[string,\Swagger\Client\Model\BedGroup]**](BedGroup.md) | A map of the room&#39;s bed groups. | [optional] 
**area** | [**\Swagger\Client\Model\Area**](Area.md) |  | [optional] 
**views** | [**map[string,\Swagger\Client\Model\View]**](View.md) | A map of the room views. See our [view reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_amenity_definitions) for current known room view ID and name values. | [optional] 
**occupancy** | [**\Swagger\Client\Model\Occupancy**](Occupancy.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


