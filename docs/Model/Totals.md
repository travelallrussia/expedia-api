# Totals

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inclusive** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 
**exclusive** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 
**strikethrough** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 
**marketing_fee** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 
**gross_profit** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 
**minimum_selling_price** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 
**property_fees** | [**\Swagger\Client\Model\Charge**](Charge.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


