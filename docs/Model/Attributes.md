# Attributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**general** | [**map[string,\Swagger\Client\Model\Attribute]**](Attribute.md) | Lists all of the general attributes about the property. | [optional] 
**pets** | [**map[string,\Swagger\Client\Model\Attribute]**](Attribute.md) | Lists all of the pet attributes about the property. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


