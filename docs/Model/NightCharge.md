# NightCharge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Swagger\Client\Model\NightChargeType**](NightChargeType.md) |  | [optional] 
**value** | **string** | The value of the amount object. Decimal point inline with correct precision. | [optional] 
**currency** | **string** | Currency of the amount object. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


