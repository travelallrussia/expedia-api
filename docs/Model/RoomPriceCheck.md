# RoomPriceCheck

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**\Swagger\Client\Model\StatusPriceCheck**](StatusPriceCheck.md) |  | [optional] 
**occupancy_pricing** | [**map[string,\Swagger\Client\Model\PricingInformation]**](PricingInformation.md) | A map of room information by occupancy. | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to continue booking this rate or to shop for additional rates.  If this rate is still available for booking then a book link will be present if PSD2 is not a requirement for you or a payment_session link will be present if PSD2 is a requirement for you. | [optional] 
**card_on_file_limit** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**refundable_damage_deposit** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**deposits** | [**\Swagger\Client\Model\Deposit[]**](Deposit.md) | Array of deposits. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


