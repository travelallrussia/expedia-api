# NonrefundableDateRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | **string** | Start date of nonrefundable date range in ISO 8601 format. | [optional] 
**end** | **string** | End date of nonrefundable date range in ISO 8601 format. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


