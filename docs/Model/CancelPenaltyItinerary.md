# CancelPenaltyItinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**percent** | **string** | The percentage of the cancel penalty. A thirty percent penalty would be returned as 30%. | [optional] 
**start** | **string** | The start date/time of the cancel penalty. | [optional] 
**end** | **string** | The end date/time of the cancel penalty. | [optional] 
**amount** | **string** | The monetary amount of the penalty. | [optional] 
**currency** | **string** | The currency of the cancel penalty. | [optional] 
**nights** | **string** | The number of nights that the cancel penalty applies to. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


