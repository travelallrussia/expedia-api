# Statistic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The statistic definition ID for this statistic. | [optional] 
**name** | **string** | Statistic name. | [optional] 
**value** | **string** | Statistic value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


