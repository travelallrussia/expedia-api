# ConfirmationId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expedia** | **string** | The expedia confirmation id. | [optional] 
**property** | **string** | The property confirmation id. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


