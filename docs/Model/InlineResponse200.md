# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **string** | The access token that can be used for subsequent API calls. | [optional] 
**token_type** | **string** | The type of access token being returned. This will always be &#x60;\&quot;bearer\&quot;&#x60;. | [optional] 
**expires_in** | **float** | The number of seconds before the access token expires. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


