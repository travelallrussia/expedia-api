# Occupancy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_allowed** | [**\Swagger\Client\Model\MaxAllowed**](MaxAllowed.md) |  | [optional] 
**age_categories** | [**map[string,\Swagger\Client\Model\CategoryAge]**](CategoryAge.md) | Map of the age categories used to determine the maximum children and adult occupancy. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


