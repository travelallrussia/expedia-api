# DepositItinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **string** | The currency of the deposit. | [optional] 
**value** | **string** | The amount required as deposit. | [optional] 
**due** | **string** | The due date/time of the deposit. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


