# CompletePaymentSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itinerary_id** | **string** | The itinerary id. | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to retrieve a booking, resume a held booking, or cancel a held booking. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


