# Deposit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** | The amount that must be paid as a deposit. | [optional] 
**due** | **string** | The due date in ISO 8601 format. | [optional] 
**currency** | **string** | The currency for the deposit amount. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


