# RoomAvailability

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique Identifier for a room type. | [optional] 
**room_name** | **string** | Name of the room type. | [optional] 
**rates** | [**\Swagger\Client\Model\Rate[]**](Rate.md) | Array of objects containing rate information. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


