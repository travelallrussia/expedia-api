# ValueAdd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique identifier for the value add promotion. | [optional] 
**description** | **string** | A localized description of the value add promotion. | [optional] 
**category** | [**\Swagger\Client\Model\CategoryValueAdd**](CategoryValueAdd.md) |  | [optional] 
**offer_type** | [**\Swagger\Client\Model\OfferType**](OfferType.md) |  | [optional] 
**frequency** | [**\Swagger\Client\Model\Frequency**](Frequency.md) |  | [optional] 
**person_count** | **float** | Indicates how many guests the value add promotion applies to. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


