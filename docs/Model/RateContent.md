# RateContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique identifier for a rate. | [optional] 
**amenities** | [**map[string,\Swagger\Client\Model\Amenity]**](Amenity.md) | This lists all of the Amenities available with a specific rate, including value adds, such as breakfast. See our [amenities reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_amenity_definitions) for current known amenity ID and name values. | [optional] 
**special_offer_description** | **string** | A text description of any special offers for this rate. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


