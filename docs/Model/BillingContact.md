# BillingContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **string** | First/given name of the payment type account holder. | [optional] 
**family_name** | **string** | Last/family name of the payment type account holder. | [optional] 
**address** | [**\Swagger\Client\Model\AddressBillingContact**](AddressBillingContact.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


