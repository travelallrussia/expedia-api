# PaymentSessions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_session_id** | **string** | The registered payment session ID. | [optional] 
**encoded_init_config** | **string** | A base64 encoded object which contains configuration needed to perform device fingerprinting. It is used in conjunction with the provided Javascript library for PSD2. | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to create a booking. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


