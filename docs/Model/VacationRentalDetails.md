# VacationRentalDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registry_number** | **string** | The property&#39;s registry number required by some jurisdictions. | [optional] 
**private_host** | **bool** | Indicates if a property has a private host. | [optional] 
**property_manager** | [**\Swagger\Client\Model\PropertyManager**](PropertyManager.md) |  | [optional] 
**rental_agreement** | [**\Swagger\Client\Model\RentalAgreement**](RentalAgreement.md) |  | [optional] 
**house_rules** | **string[]** | List of strings detailing house rules. | [optional] 
**amenities** | [**\Swagger\Client\Model\Amenity**](Amenity.md) | Map of amenities specific to vacation rental properties. | [optional] 
**vrbo_srp_id** | **string** | The Vrbo srp needed for link-off. | [optional] 
**listing_id** | **string** | The listing id for a Vrbo property. | [optional] 
**listing_number** | **string** | The listing number for a Vrbo property. | [optional] 
**listing_source** | **string** | The listing source. | [optional] 
**listing_unit** | **string** | The specific unit. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


