# Review

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | **string** | Where this review has been verified from. | [optional] 
**title** | **string** | Title of this review. | [optional] 
**date_submitted** | **string** | When this review was made, in ISO 8601 format. | [optional] 
**rating** | **string** | The rating for this property given by the reviewer. Returns a value between 1.0 and 5.0. | [optional] 
**reviewer_name** | **string** | The name of the person who wrote this review. | [optional] 
**trip_reason** | [**\Swagger\Client\Model\TripReason[]**](TripReason.md) | An array of strings containing trip reasons. | [optional] 
**travel_companion** | [**\Swagger\Client\Model\TravelCompanion[]**](TravelCompanion.md) | An array of strings containing travel companions. | [optional] 
**text** | **string** | The text of the review itself. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


