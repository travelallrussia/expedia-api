# Conversations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | Contains urls for links to initiate conversations via EPS. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


