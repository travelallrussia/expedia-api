# AttributeDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier for the attribute definition. | [optional] 
**name** | **string** | Attribute definition name. | [optional] 
**type** | **string** | Attribute definition type. | [optional] 
**has_value** | **bool** | Boolean to indicate if the attribute will have a value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


