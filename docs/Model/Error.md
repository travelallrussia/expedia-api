# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The error type. | [optional] 
**message** | **string** | A human readable message giving details about this error. | [optional] 
**fields** | [**\Swagger\Client\Model\Field[]**](Field.md) | Details about the specific fields that had an error. | [optional] 
**errors** | [**\Swagger\Client\Model\ErrorIndividual[]**](ErrorIndividual.md) | An array of all the actual errors that occured. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


