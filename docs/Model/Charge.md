# Charge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billable_currency** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**request_currency** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


