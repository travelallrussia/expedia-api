# FeesPricingInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mandatory_fee** | [**\Swagger\Client\Model\ChargeCalculated**](ChargeCalculated.md) |  | [optional] 
**resort_fee** | [**\Swagger\Client\Model\ChargeCalculated**](ChargeCalculated.md) |  | [optional] 
**mandatory_tax** | [**\Swagger\Client\Model\ChargeCalculated**](ChargeCalculated.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


