# Promotions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value_adds** | [**map[string,\Swagger\Client\Model\ValueAdd]**](ValueAdd.md) | A collection of value adds that apply to this rate. | [optional] 
**deal** | [**\Swagger\Client\Model\Deal**](Deal.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


