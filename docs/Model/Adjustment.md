# Adjustment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** | The amount of the adjustment. | [optional] 
**type** | **string** | The type of the adjustment. | [optional] 
**currency** | **string** | The currency of the adjustment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


