# PaymentOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affiliate_collect** | [**\Swagger\Client\Model\AffiliateCollect**](AffiliateCollect.md) |  | [optional] 
**credit_card** | [**\Swagger\Client\Model\CreditCard**](CreditCard.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


