# StatisticDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier for the statistic definition. | [optional] 
**name** | **string** | Statistic definition name. | [optional] 
**has_value** | **bool** | Boolean to indicate if the statistic will have a value. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


