# Rate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique Identifier for a rate. | [optional] 
**status** | [**\Swagger\Client\Model\Status**](Status.md) |  | [optional] 
**available_rooms** | **float** | The number of bookable rooms remaining with this rate in EPS inventory. Use this value to create rules for urgency messaging to alert users to low availability on busy travel dates or at popular properties. If the value returns as 2147483647 (max int value), the actual value could not be determined. Ensure your urgency messaging ignores such instances when returned. | [optional] 
**refundable** | **bool** | Indicates if the rate is fully refundable at the time of booking. Cancel penalties may still apply. Please refer to the cancel penalties section for reference. | [optional] 
**member_deal_available** | **bool** | Indicates if a \&quot;Member Only Deal\&quot; is available for this rate. | [optional] 
**sale_scenario** | [**\Swagger\Client\Model\SaleScenario**](SaleScenario.md) |  | [optional] 
**merchant_of_record** | [**\Swagger\Client\Model\MerchantOfRecord**](MerchantOfRecord.md) |  | [optional] 
**amenities** | [**map[string,\Swagger\Client\Model\Amenity]**](Amenity.md) | Room amenities. | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including a link to request payment options. | [optional] 
**bed_groups** | [**map[string,\Swagger\Client\Model\BedGroupAvailability]**](BedGroupAvailability.md) | A map of the room&#39;s bed groups. | [optional] 
**cancel_penalties** | [**\Swagger\Client\Model\CancelPenalty[]**](CancelPenalty.md) | Array of &#x60;cancel_penalty&#x60; objects containing cancel penalty information. Will not be populated on non refundable. | [optional] 
**nonrefundable_date_ranges** | [**\Swagger\Client\Model\NonrefundableDateRange[]**](NonrefundableDateRange.md) | A list of date exceptions. Dates within these ranges provide no refund on cancellation, regardless of cancel penalty windows. | [optional] 
**occupancy_pricing** | [**map[string,\Swagger\Client\Model\PricingInformation]**](PricingInformation.md) | A map of room information by occupancy. | [optional] 
**promotions** | [**\Swagger\Client\Model\Promotions**](Promotions.md) |  | [optional] 
**card_on_file_limit** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**refundable_damage_deposit** | [**\Swagger\Client\Model\Amount**](Amount.md) |  | [optional] 
**deposits** | [**\Swagger\Client\Model\Deposit[]**](Deposit.md) | Array of deposits for the rate. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


