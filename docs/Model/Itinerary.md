# Itinerary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itinerary_id** | **string** | The itinerary id. | [optional] 
**property_id** | **string** | The property id. | [optional] 
**links** | [**map[string,\Swagger\Client\Model\Link]**](Link.md) | A map of links, including links to resume or cancel a held booking. This is only included for held bookings. | [optional] 
**email** | **string** | Email address for the customer. | [optional] 
**phone** | [**\Swagger\Client\Model\Phone**](Phone.md) |  | [optional] 
**rooms** | [**\Swagger\Client\Model\RoomItinerary[]**](RoomItinerary.md) |  | [optional] 
**billing_contact** | [**\Swagger\Client\Model\BillingContact**](BillingContact.md) |  | [optional] 
**adjustment** | [**\Swagger\Client\Model\Adjustment**](Adjustment.md) |  | [optional] 
**creation_date_time** | **string** | The creation date/time of the booking. | [optional] 
**affiliate_reference_id** | **string** | Your unique reference value. This field supports a maximum of 28 characters. | [optional] 
**affiliate_metadata** | **string** | Field that stores up to 256 characters of additional metadata with the itinerary, uniqueness is not required. | [optional] 
**conversations** | [**\Swagger\Client\Model\Conversations**](Conversations.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


