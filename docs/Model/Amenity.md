# Amenity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The amenity definition ID for this amenity. | [optional] 
**name** | **string** | Amenity name. | [optional] 
**value** | **string** | Amenity value. | [optional] 
**categories** | [**\Swagger\Client\Model\CategoryAmenity[]**](CategoryAmenity.md) | This is an optional field and will be present only if the amnity falls into one or more of the these amenity categories. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


