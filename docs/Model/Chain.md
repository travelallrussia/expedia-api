# Chain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Chain id. | [optional] 
**name** | **string** | Chain name. | [optional] 
**brands** | [**map[string,\Swagger\Client\Model\Brand]**](Brand.md) | Map of the chain&#39;s brands. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


