# OnsitePayments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **string** | The currency accepted at the property. | [optional] 
**types** | [**map[string,\Swagger\Client\Model\PaymentType]**](PaymentType.md) | The types of payments accepted at the property. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


