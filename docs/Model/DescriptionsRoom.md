# DescriptionsRoom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**overview** | **string** | Provides an overview of a room. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


