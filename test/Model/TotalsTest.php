<?php
/**
 * TotalsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Rapid
 *
 * EPS Rapid V3
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.29
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * TotalsTest Class Doc Comment
 *
 * @category    Class
 * @description The total price of charges, given various critera.  Inclusive provides the total price including taxes and fees.  This does not include hotel collected fees such as resort, mandatory taxes, and mandatory fees. Exclusive provides the total price excluding taxes and fees. Strikethrough provides the tax exclusive total price with any hotel funded discounts added back. Can be used to merchandise the savings due to a discount. Marketing fee provides the potential owed earnings per transaction. Gross profit provides the estimated gross profit per transaction. Minimum selling price provides the minimum selling price. Property fees provides the total of the fees collected by the property.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class TotalsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Totals"
     */
    public function testTotals()
    {
    }

    /**
     * Test attribute "inclusive"
     */
    public function testPropertyInclusive()
    {
    }

    /**
     * Test attribute "exclusive"
     */
    public function testPropertyExclusive()
    {
    }

    /**
     * Test attribute "strikethrough"
     */
    public function testPropertyStrikethrough()
    {
    }

    /**
     * Test attribute "marketing_fee"
     */
    public function testPropertyMarketingFee()
    {
    }

    /**
     * Test attribute "gross_profit"
     */
    public function testPropertyGrossProfit()
    {
    }

    /**
     * Test attribute "minimum_selling_price"
     */
    public function testPropertyMinimumSellingPrice()
    {
    }

    /**
     * Test attribute "property_fees"
     */
    public function testPropertyPropertyFees()
    {
    }
}
