<?php
/**
 * ThirdPartyAuthRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Rapid
 *
 * EPS Rapid V3
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.29
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * ThirdPartyAuthRequestTest Class Doc Comment
 *
 * @category    Class
 * @description ThirdPartyAuthRequest
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ThirdPartyAuthRequestTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ThirdPartyAuthRequest"
     */
    public function testThirdPartyAuthRequest()
    {
    }

    /**
     * Test attribute "cavv"
     */
    public function testPropertyCavv()
    {
    }

    /**
     * Test attribute "eci"
     */
    public function testPropertyEci()
    {
    }

    /**
     * Test attribute "three_ds_version"
     */
    public function testPropertyThreeDsVersion()
    {
    }

    /**
     * Test attribute "ds_transaction_id"
     */
    public function testPropertyDsTransactionId()
    {
    }

    /**
     * Test attribute "pa_res_status"
     */
    public function testPropertyPaResStatus()
    {
    }

    /**
     * Test attribute "ve_res_status"
     */
    public function testPropertyVeResStatus()
    {
    }

    /**
     * Test attribute "xid"
     */
    public function testPropertyXid()
    {
    }

    /**
     * Test attribute "cavv_algorithm"
     */
    public function testPropertyCavvAlgorithm()
    {
    }

    /**
     * Test attribute "ucaf_indicator"
     */
    public function testPropertyUcafIndicator()
    {
    }
}
