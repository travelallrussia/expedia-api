<?php
/**
 * RoomPriceCheckTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Rapid
 *
 * EPS Rapid V3
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.29
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * RoomPriceCheckTest Class Doc Comment
 *
 * @category    Class
 * @description The price check response.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RoomPriceCheckTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "RoomPriceCheck"
     */
    public function testRoomPriceCheck()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "occupancy_pricing"
     */
    public function testPropertyOccupancyPricing()
    {
    }

    /**
     * Test attribute "links"
     */
    public function testPropertyLinks()
    {
    }

    /**
     * Test attribute "card_on_file_limit"
     */
    public function testPropertyCardOnFileLimit()
    {
    }

    /**
     * Test attribute "refundable_damage_deposit"
     */
    public function testPropertyRefundableDamageDeposit()
    {
    }

    /**
     * Test attribute "deposits"
     */
    public function testPropertyDeposits()
    {
    }
}
