<?php
/**
 * PropertyContentTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Rapid
 *
 * EPS Rapid V3
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.29
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * PropertyContentTest Class Doc Comment
 *
 * @category    Class
 * @description An individual property object in the map of property objects.
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PropertyContentTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "PropertyContent"
     */
    public function testPropertyContent()
    {
    }

    /**
     * Test attribute "property_id"
     */
    public function testPropertyPropertyId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "address"
     */
    public function testPropertyAddress()
    {
    }

    /**
     * Test attribute "ratings"
     */
    public function testPropertyRatings()
    {
    }

    /**
     * Test attribute "location"
     */
    public function testPropertyLocation()
    {
    }

    /**
     * Test attribute "phone"
     */
    public function testPropertyPhone()
    {
    }

    /**
     * Test attribute "fax"
     */
    public function testPropertyFax()
    {
    }

    /**
     * Test attribute "category"
     */
    public function testPropertyCategory()
    {
    }

    /**
     * Test attribute "business_model"
     */
    public function testPropertyBusinessModel()
    {
    }

    /**
     * Test attribute "rank"
     */
    public function testPropertyRank()
    {
    }

    /**
     * Test attribute "checkin"
     */
    public function testPropertyCheckin()
    {
    }

    /**
     * Test attribute "checkout"
     */
    public function testPropertyCheckout()
    {
    }

    /**
     * Test attribute "fees"
     */
    public function testPropertyFees()
    {
    }

    /**
     * Test attribute "policies"
     */
    public function testPropertyPolicies()
    {
    }

    /**
     * Test attribute "attributes"
     */
    public function testPropertyAttributes()
    {
    }

    /**
     * Test attribute "amenities"
     */
    public function testPropertyAmenities()
    {
    }

    /**
     * Test attribute "images"
     */
    public function testPropertyImages()
    {
    }

    /**
     * Test attribute "onsite_payments"
     */
    public function testPropertyOnsitePayments()
    {
    }

    /**
     * Test attribute "rooms"
     */
    public function testPropertyRooms()
    {
    }

    /**
     * Test attribute "rates"
     */
    public function testPropertyRates()
    {
    }

    /**
     * Test attribute "dates"
     */
    public function testPropertyDates()
    {
    }

    /**
     * Test attribute "descriptions"
     */
    public function testPropertyDescriptions()
    {
    }

    /**
     * Test attribute "statistics"
     */
    public function testPropertyStatistics()
    {
    }

    /**
     * Test attribute "airports"
     */
    public function testPropertyAirports()
    {
    }

    /**
     * Test attribute "themes"
     */
    public function testPropertyThemes()
    {
    }

    /**
     * Test attribute "all_inclusive"
     */
    public function testPropertyAllInclusive()
    {
    }

    /**
     * Test attribute "tax_id"
     */
    public function testPropertyTaxId()
    {
    }

    /**
     * Test attribute "chain"
     */
    public function testPropertyChain()
    {
    }

    /**
     * Test attribute "brand"
     */
    public function testPropertyBrand()
    {
    }

    /**
     * Test attribute "spoken_languages"
     */
    public function testPropertySpokenLanguages()
    {
    }

    /**
     * Test attribute "multi_unit"
     */
    public function testPropertyMultiUnit()
    {
    }

    /**
     * Test attribute "payment_registration_recommended"
     */
    public function testPropertyPaymentRegistrationRecommended()
    {
    }

    /**
     * Test attribute "vacation_rental_details"
     */
    public function testPropertyVacationRentalDetails()
    {
    }

    /**
     * Test attribute "supply_source"
     */
    public function testPropertySupplySource()
    {
    }
}
