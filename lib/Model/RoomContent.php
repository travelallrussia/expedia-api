<?php
/**
 * RoomContent
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Rapid
 *
 * EPS Rapid V3
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.29
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * RoomContent Class Doc Comment
 *
 * @category Class
 * @description An individual room.
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RoomContent implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'RoomContent';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'string',
        'name' => 'string',
        'descriptions' => '\Swagger\Client\Model\DescriptionsRoom',
        'amenities' => 'map[string,\Swagger\Client\Model\Amenity]',
        'images' => '\Swagger\Client\Model\Image[]',
        'bed_groups' => 'map[string,\Swagger\Client\Model\BedGroup]',
        'area' => '\Swagger\Client\Model\Area',
        'views' => 'map[string,\Swagger\Client\Model\View]',
        'occupancy' => '\Swagger\Client\Model\Occupancy'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => null,
        'name' => null,
        'descriptions' => null,
        'amenities' => null,
        'images' => null,
        'bed_groups' => null,
        'area' => null,
        'views' => null,
        'occupancy' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'name' => 'name',
        'descriptions' => 'descriptions',
        'amenities' => 'amenities',
        'images' => 'images',
        'bed_groups' => 'bed_groups',
        'area' => 'area',
        'views' => 'views',
        'occupancy' => 'occupancy'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'name' => 'setName',
        'descriptions' => 'setDescriptions',
        'amenities' => 'setAmenities',
        'images' => 'setImages',
        'bed_groups' => 'setBedGroups',
        'area' => 'setArea',
        'views' => 'setViews',
        'occupancy' => 'setOccupancy'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'name' => 'getName',
        'descriptions' => 'getDescriptions',
        'amenities' => 'getAmenities',
        'images' => 'getImages',
        'bed_groups' => 'getBedGroups',
        'area' => 'getArea',
        'views' => 'getViews',
        'occupancy' => 'getOccupancy'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['descriptions'] = isset($data['descriptions']) ? $data['descriptions'] : null;
        $this->container['amenities'] = isset($data['amenities']) ? $data['amenities'] : null;
        $this->container['images'] = isset($data['images']) ? $data['images'] : null;
        $this->container['bed_groups'] = isset($data['bed_groups']) ? $data['bed_groups'] : null;
        $this->container['area'] = isset($data['area']) ? $data['area'] : null;
        $this->container['views'] = isset($data['views']) ? $data['views'] : null;
        $this->container['occupancy'] = isset($data['occupancy']) ? $data['occupancy'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string $id Unique identifier for a room type.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name Room type name.
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets descriptions
     *
     * @return \Swagger\Client\Model\DescriptionsRoom
     */
    public function getDescriptions()
    {
        return $this->container['descriptions'];
    }

    /**
     * Sets descriptions
     *
     * @param \Swagger\Client\Model\DescriptionsRoom $descriptions descriptions
     *
     * @return $this
     */
    public function setDescriptions($descriptions)
    {
        $this->container['descriptions'] = $descriptions;

        return $this;
    }

    /**
     * Gets amenities
     *
     * @return map[string,\Swagger\Client\Model\Amenity]
     */
    public function getAmenities()
    {
        return $this->container['amenities'];
    }

    /**
     * Sets amenities
     *
     * @param map[string,\Swagger\Client\Model\Amenity] $amenities Lists all of the amenities available in the room. See our [amenities reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_amenity_definitions) for current known amenity ID and name values.
     *
     * @return $this
     */
    public function setAmenities($amenities)
    {
        $this->container['amenities'] = $amenities;

        return $this;
    }

    /**
     * Gets images
     *
     * @return \Swagger\Client\Model\Image[]
     */
    public function getImages()
    {
        return $this->container['images'];
    }

    /**
     * Sets images
     *
     * @param \Swagger\Client\Model\Image[] $images The room's images. Contains all room images available.
     *
     * @return $this
     */
    public function setImages($images)
    {
        $this->container['images'] = $images;

        return $this;
    }

    /**
     * Gets bed_groups
     *
     * @return map[string,\Swagger\Client\Model\BedGroup]
     */
    public function getBedGroups()
    {
        return $this->container['bed_groups'];
    }

    /**
     * Sets bed_groups
     *
     * @param map[string,\Swagger\Client\Model\BedGroup] $bed_groups A map of the room's bed groups.
     *
     * @return $this
     */
    public function setBedGroups($bed_groups)
    {
        $this->container['bed_groups'] = $bed_groups;

        return $this;
    }

    /**
     * Gets area
     *
     * @return \Swagger\Client\Model\Area
     */
    public function getArea()
    {
        return $this->container['area'];
    }

    /**
     * Sets area
     *
     * @param \Swagger\Client\Model\Area $area area
     *
     * @return $this
     */
    public function setArea($area)
    {
        $this->container['area'] = $area;

        return $this;
    }

    /**
     * Gets views
     *
     * @return map[string,\Swagger\Client\Model\View]
     */
    public function getViews()
    {
        return $this->container['views'];
    }

    /**
     * Sets views
     *
     * @param map[string,\Swagger\Client\Model\View] $views A map of the room views. See our [view reference](https://developer.expediapartnersolutions.com/documentation/rapid-content-docs-v3/#/Content/get_amenity_definitions) for current known room view ID and name values.
     *
     * @return $this
     */
    public function setViews($views)
    {
        $this->container['views'] = $views;

        return $this;
    }

    /**
     * Gets occupancy
     *
     * @return \Swagger\Client\Model\Occupancy
     */
    public function getOccupancy()
    {
        return $this->container['occupancy'];
    }

    /**
     * Sets occupancy
     *
     * @param \Swagger\Client\Model\Occupancy $occupancy occupancy
     *
     * @return $this
     */
    public function setOccupancy($occupancy)
    {
        $this->container['occupancy'] = $occupancy;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


