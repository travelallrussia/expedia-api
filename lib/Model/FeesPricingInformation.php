<?php
/**
 * FeesPricingInformation
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Rapid
 *
 * EPS Rapid V3
 *
 * OpenAPI spec version: v3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.29
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * FeesPricingInformation Class Doc Comment
 *
 * @category Class
 * @description The fees collected by the property. The values for each type of fee are the total for that type.  Mandatory fees are collected by the property at check-in or check-out. Resort fees are charged for amenities and extras and collected by the property at check-in or check-out. Mandatory taxes are taxes collected by the property at check-in or check-out.
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class FeesPricingInformation implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'FeesPricingInformation';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'mandatory_fee' => '\Swagger\Client\Model\ChargeCalculated',
        'resort_fee' => '\Swagger\Client\Model\ChargeCalculated',
        'mandatory_tax' => '\Swagger\Client\Model\ChargeCalculated'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'mandatory_fee' => null,
        'resort_fee' => null,
        'mandatory_tax' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'mandatory_fee' => 'mandatory_fee',
        'resort_fee' => 'resort_fee',
        'mandatory_tax' => 'mandatory_tax'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'mandatory_fee' => 'setMandatoryFee',
        'resort_fee' => 'setResortFee',
        'mandatory_tax' => 'setMandatoryTax'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'mandatory_fee' => 'getMandatoryFee',
        'resort_fee' => 'getResortFee',
        'mandatory_tax' => 'getMandatoryTax'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['mandatory_fee'] = isset($data['mandatory_fee']) ? $data['mandatory_fee'] : null;
        $this->container['resort_fee'] = isset($data['resort_fee']) ? $data['resort_fee'] : null;
        $this->container['mandatory_tax'] = isset($data['mandatory_tax']) ? $data['mandatory_tax'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets mandatory_fee
     *
     * @return \Swagger\Client\Model\ChargeCalculated
     */
    public function getMandatoryFee()
    {
        return $this->container['mandatory_fee'];
    }

    /**
     * Sets mandatory_fee
     *
     * @param \Swagger\Client\Model\ChargeCalculated $mandatory_fee mandatory_fee
     *
     * @return $this
     */
    public function setMandatoryFee($mandatory_fee)
    {
        $this->container['mandatory_fee'] = $mandatory_fee;

        return $this;
    }

    /**
     * Gets resort_fee
     *
     * @return \Swagger\Client\Model\ChargeCalculated
     */
    public function getResortFee()
    {
        return $this->container['resort_fee'];
    }

    /**
     * Sets resort_fee
     *
     * @param \Swagger\Client\Model\ChargeCalculated $resort_fee resort_fee
     *
     * @return $this
     */
    public function setResortFee($resort_fee)
    {
        $this->container['resort_fee'] = $resort_fee;

        return $this;
    }

    /**
     * Gets mandatory_tax
     *
     * @return \Swagger\Client\Model\ChargeCalculated
     */
    public function getMandatoryTax()
    {
        return $this->container['mandatory_tax'];
    }

    /**
     * Sets mandatory_tax
     *
     * @param \Swagger\Client\Model\ChargeCalculated $mandatory_tax mandatory_tax
     *
     * @return $this
     */
    public function setMandatoryTax($mandatory_tax)
    {
        $this->container['mandatory_tax'] = $mandatory_tax;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


